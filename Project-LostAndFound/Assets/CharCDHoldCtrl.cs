using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThreeC;
public class CharCDHoldCtrl : MonoBehaviour
{
  public GameObject CDObj;

  public void Start() {
    var interactable = GameManager.Instance.playerController.currentInteractable;
    SetHoldCD(false);
  }

  public void Update() {
    var interactable = GameManager.Instance.playerController.currentInteractable;
    if(interactable != null) 
    {
      interactable.OnInteractSuccessEvent.AddListener(HandleCDSucEvent); 
    }
  }

  private void HandleCDSucEvent(InteractableBehaviour interactable) {
    if (interactable.gameObject.name.Contains("CD")) {
      SetHoldCD(true);
    } else {
      SetHoldCD(false);
    }
  }
  public void SetHoldCD(bool isHolding) 
  {
    if (CDObj != null)
      CDObj.SetActive(isHolding);
  }
}
