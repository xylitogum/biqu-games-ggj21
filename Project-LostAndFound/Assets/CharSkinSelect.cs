using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.SceneManagement;
public class CharSkinSelect : MonoBehaviour
{
  private List<SpriteResolver> spriteResolvers = new List<SpriteResolver>();
  private string cachedSceneName = null;
    public int skinIndex;
  public void Start() {
    InitIfNecessary();
    TriggerChangeSkin();
    //SceneManager.activeSceneChanged += TriggerChangeSkin;
  }


  private void Update() {
    TriggerChangeSkin();
  }
  public void TriggerChangeSkin() 
  {
    
    InitIfNecessary();

        ChangeSkin(skinIndex.ToString());
        /*
    var name = GameManager.Instance.GetCurrentLevelSceneName();
    //Debug.Log("Triggered with name" + name);
    if (name != cachedSceneName) 
    {
      string num = ""+name[name.Length - 1];
      ChangeSkin(num);
      cachedSceneName = name;
    }*/
    
  }

  

  private void InitIfNecessary() 
  {
    if (spriteResolvers.Count == 0 || spriteResolvers[0] == null)
      spriteResolvers = new List<SpriteResolver>(GetComponentsInChildren<SpriteResolver>());
  }

  private void ChangeSkin(string sceneNum) 
  {
    //Debug.Log("change to " + sceneNum);
    InitIfNecessary();
    foreach(var part in spriteResolvers) 
    {
      part.SetCategoryAndLabel(part.GetCategory(), sceneNum);
    }
  }
}
