using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using ThreeC;
public class HintCtrl : MonoBehaviour
{
  [SerializeField]
  private GameObject directionHintLeft;
  [SerializeField]
  private GameObject directionHintRight;
  [SerializeField]
  private GameObject missionHint;
  [SerializeField]
  private GameObject interactHint;
  // Start is called before the first frame update
  private GameObject secondInteractHint = null;

  

  private ThreeC.PlayerController charCtrl = null;
  [Header("Offset")]
  [SerializeField]
  private float InteractVerticalOffset = 2f;
  [SerializeField]
  private float InteractHorizontalOffset = 2f;
  [Header("Prefab")]
  [SerializeField]
  private GameObject secondInteractHintPrefab;
  private bool isSecondInteractOn = false;
  public enum DirectionHint
  {
    None = 0,
    Left = 1,
    Right = 2,
    All = 3,
  }
  public void Init() 
  {
    charCtrl = GameManager.Instance.playerController;
    if (charCtrl == null) charCtrl = FindObjectOfType<PlayerController>();
    directionHintLeft.SetActive(false);
    directionHintRight.SetActive(false);
    missionHint.SetActive(false);
    interactHint.SetActive(false);
    Register();
    
  }

  private void Update() {
    //var interactTar = GameManager.Instance.playerController.currentInteractable;
    //if(interactTar != null) 
    //{
      
    //}
  }
  //public void Start() 
  //{
  //  DisplayDirHint(DirectionHint.Left);
  //  DisplayInteractHint(new Vector3(0, -0.5f, 0), true);
  //  DisplayMissionHint("new Mission", true);
  //}
  // Update is called once per frame
  private void Register() 
  {
    if (charCtrl == null) Debug.Log("NULLLL");
    charCtrl.onInteractTimeChangeEvent.AddListener(InteractTimeChangeHandle); 
    charCtrl.onInteractButtonStateChangeEvent.AddListener(FirstInteractEventHandle);
    charCtrl.onSecondaryButtonStateChangeEvent.AddListener(SecondInteractEventHandle);
    
  }

  public void InteractSucHandle(InteractableBehaviour tar) 
  {
    interactHint.GetComponentInChildren<ProgressRing>().SetState(ProgressRing.RingInteractState.FadeOutSuc);
    if (isSecondInteractOn) 
      secondInteractHint.GetComponentInChildren<ProgressRing>().SetState(ProgressRing.RingInteractState.FadeOutSuc);
  }
  public void InteractFailHandle(InteractableBehaviour tar) 
  {
    interactHint.GetComponentInChildren<ProgressRing>().SetState(ProgressRing.RingInteractState.FadeOutFail);
    if (isSecondInteractOn) 
      secondInteractHint.GetComponentInChildren<ProgressRing>().SetState(ProgressRing.RingInteractState.FadeOutFail);
  }
  private void InteractTimeChangeHandle(bool isInteracting, float progress) 
  {
    var ringCtrl = interactHint.GetComponentInChildren<ProgressRing>();
    if(isInteracting) 
    {
      ringCtrl.SetProgress(progress);
    }
    else 
    {
      ringCtrl.SetProgress(0);
    }
  }
  private void FirstInteractEventHandle(bool display)
  {
    var tar = charCtrl.currentInteractable;
    if(tar != null) 
    {
      tar.OnInteractSuccessEvent.AddListener(InteractSucHandle);
      tar.OnInteractFailEvent.AddListener(InteractFailHandle);
      DisplayInteractHint((tar.transform.position + Vector3.up* InteractVerticalOffset), display);
    }
    else 
    {
      DisplayInteractHint(Vector3.zero, display);
    }
  }

  private void SecondInteractEventHandle(bool display)   
  {
    //Debug.Log("second Display " + display);
    isSecondInteractOn = display;
    if(secondInteractHint == null)
      secondInteractHint = Instantiate(secondInteractHintPrefab, interactHint.transform);
    secondInteractHint.transform.position = interactHint.transform.position + Vector3.right * InteractHorizontalOffset;
    if (display) 
    {
      secondInteractHint.SetActive(true);
    } 
    else 
    {
      var ring = secondInteractHint.GetComponentInChildren<ProgressRing>();
      if (!ring.IsCurrentStateCritical()) secondInteractHint.SetActive(false);
    }
      
    
  }
  public void DisplayDirHint(DirectionHint dir)  
  {
    switch (dir) 
    {
      case DirectionHint.None:
        directionHintLeft.SetActive(false);
        directionHintRight.SetActive(false);
        break;
      case DirectionHint.Left:
        directionHintLeft.SetActive(true);
        directionHintRight.SetActive(false);
        break;
      case DirectionHint.Right:
        directionHintLeft.SetActive(false);
        directionHintRight.SetActive(true);
        break;
      case DirectionHint.All:
        directionHintLeft.SetActive(true);
        directionHintRight.SetActive(true);
        break;
      default:
        Debug.LogWarning("Wrong hint type");
        break;
    }
  }

  public void DisplayMissionHint(string content, bool isDisplay) 
  {
    var t = missionHint.GetComponentInChildren<Text>();
    t.text = content;
    missionHint.SetActive(isDisplay);
  }

  public void DisplayInteractHint(Vector3 worldPos,bool isDisplay) 
  {
    if(isDisplay) 
    {
      interactHint.SetActive(true);
    } 
    else 
    {
      var ring = interactHint.GetComponentInChildren<ProgressRing>();
      if (!ring.IsCurrentStateCritical()) interactHint.SetActive(false);
    }

    if(Vector3.Distance(worldPos,Vector3.zero) > 0.00001) 
    {
      var screenPos = Camera.main.WorldToScreenPoint(worldPos);
      var trans = interactHint.GetComponent<RectTransform>();
      trans.position = screenPos;
    }



  }

  

  //private void ChangeHintState(bool dir,bool mission, bool interact) {
  //  directionHint.SetActive(dir);
  //  missionHint.SetActive(mission);
  //  interactHint.SetActive(interact);
  //}

  //private void StartProgress() 
  //{
  //  ProgressRing ring = interactHint.GetComponent<ProgressRing>();

  //}


  
}
