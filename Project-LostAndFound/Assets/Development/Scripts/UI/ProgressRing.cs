using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ThreeC;
public class ProgressRing : MonoBehaviour
{
  private Image fillRing = null;
  private float percentage = 0f;
  private float progressInterval = 2f;
  //private bool progressFinished = false;
  private RingInteractState currentState = RingInteractState.None;
  // 

  private Coroutine currentCoroutine = null;

  private Image[] imageList = null;
  private ThreeC.InteractableBehaviour interactTar = null;
  [SerializeField]
  private float redAddtionOffset = 3f;
  private float fadeScale = 1.5f;
  private float fadeInterval = 1f;
  public enum RingInteractState
  {
    None = 0,
    Progressing,
    FadeOutSuc,
    FadeOutFail,
    Blink
  }
  
  // Start is called before the first frame update
  void Start()
  {
    percentage = 0f;
    fillRing = GetComponent<Image>();
    imageList = GetComponentsInChildren<Image>();
    currentState = RingInteractState.None;
    
    StartCoroutine(FadeOut(2,1,false));
  }

  // Update is called once per frame
  void Update()
  {
    //if (Input.GetKeyDown(KeyCode.E)) 
    //{
    //  progressFinished = false;
    //}
    //if (Input.GetKey(KeyCode.E)) 
    //{
    //  if(!progressFinished)
    //  {
    //    percentage += Time.deltaTime / progressInterval;
    //    SetProgress(percentage);
    //    if (percentage >= 1f) 
    //    {
    //      progressFinished = true;
    //      ResetProgress();
    //    }
    //  }
      

    //} 
    //else
    //{
    //  ResetProgress();
    //}
  }
  
  //public void SetTar(ThreeC.InteractableBehaviour tar) 
  //{
  //  interactTar = tar;
    
  //}

  //public void InteractSucHandle(PlayerController ctrl, InteractableBehaviour tar) 
  //{
  //  SetState(RingInteractState.FadeOutSuc);
  //}
  //public void InteractFailHandle(PlayerController ctrl, InteractableBehaviour tar) 
  //{
  //  SetState(RingInteractState.FadeOutFail);
  //}
  
  private void ResetState() 
  {
    foreach(var img in imageList) 
    {
      img.color = Color.white;
      
    }
    transform.localScale = Vector3.one;
    currentState = RingInteractState.None;
  }
  public void SetProgress(float percent) 
  {
    if (percent > 1) SetState(RingInteractState.Blink);
    fillRing.fillAmount = percent;
    percentage = percent;
  }

  private void ResetProgress() 
  {
    percentage = 0f;
    fillRing.fillAmount = 0f;
  }

  public void StartProgress(bool isStart,float interval = 2f) 
  {
    progressInterval = interval;
  }

  private IEnumerator Blink(float interval) 
  {
    bool isOn = true;
    while(currentState == RingInteractState.Blink) 
    {
      foreach(var img in imageList) 
      {
        img.enabled = isOn;
      }
      isOn = !isOn;
      yield return new WaitForSeconds(interval);

    }


  }

  public void SetState(RingInteractState state) 
  {
    ResetState();
    currentState = state;
    if(currentCoroutine != null)StopCoroutine(currentCoroutine);
    Coroutine temp = null;
    switch (state) 
    {
      case RingInteractState.Blink:
        temp = StartCoroutine(Blink(fadeInterval));
        break;
      case RingInteractState.FadeOutFail:
        temp = StartCoroutine(FadeOut(fadeScale, fadeInterval, false));
        break;
      case RingInteractState.FadeOutSuc:
        temp = StartCoroutine(FadeOut(fadeScale, fadeInterval, true));
        break;        
      default:
        break;
    }
  }

  public bool IsCurrentStateCritical() 
  {
    return currentState != RingInteractState.None;
  }
  private IEnumerator FadeOut(float scale, float interval, bool success)
  {
    float timer = 0f;
    if(success) 
    {
      while(currentState == RingInteractState.FadeOutSuc) 
      {
        timer += Time.deltaTime;
        var ratio = Mathf.Clamp(timer / interval,0,1);
        if (ratio - 0.9999999 >= 0) 
        {
          ResetState();
          yield break;
        }
        transform.localScale = Vector3.one * (1 + (1 + ratio) * scale / 2);
        var alpha = (1 - ratio);
        foreach (var img in imageList) 
        {
          Color temp = img.color;
          temp.a = alpha;
          img.color = temp;
        }
        yield return new WaitForEndOfFrame();
      }
    } 
    else 
    {
      while (currentState == RingInteractState.FadeOutFail) 
      {
        timer += Time.deltaTime;
        var ratio = Mathf.Clamp(timer / interval, 0, 1);
        if (ratio - 0.9999999 >= 0) {
          ResetState();
          yield break;
        }
        var alpha = 1 * (1 - ratio);
        var addtionRed = redAddtionOffset * (ratio);
        
        foreach(var img in imageList) 
        {
          Color temp = Color.white;
          temp.r = temp.r + addtionRed;
          
          var tempvec = Vector3.Normalize(new Vector3(temp.r, temp.g, temp.b));
          var finalColor = new Color(tempvec.x, tempvec.y, tempvec.z);
          finalColor.a = alpha;
          img.color = finalColor;
        }
        yield return new WaitForEndOfFrame();
      }

    }
  }
  

  
}
