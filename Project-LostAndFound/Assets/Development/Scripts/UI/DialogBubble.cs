using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogBubble : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bindObject;
    [SerializeField]
    private Text t;
  [SerializeField]
  private float horizontalPadding = 10f;
  [SerializeField]
  private float verticalPadding = 10f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SizeFit();
        if(bindObject != null) 
        {
          var screenPos = Camera.main.WorldToScreenPoint(bindObject.transform.position);
          var trans = GetComponent<RectTransform>();
          trans.position = screenPos;
        }        
    }
    
    private void SizeFit() 
    {
      var rect = this.GetComponent<RectTransform>();
      rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, t.preferredWidth + horizontalPadding*2);
      rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, t.preferredHeight + verticalPadding*2);
    }
}
