using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
public class HUD : MonoBehaviour
{
  [SerializeField]
  private HintCtrl hintCtrl;
  [SerializeField]
  private Transform dialogContainer;
  [SerializeField]
  private GameObject dialogPrefab;
  [SerializeField]
    private GameObject endingPanel;

    private static HUD instance = null;
  public static HUD GetInstance() {
    if (instance == null) instance = GameObject.FindObjectOfType<HUD>();
    return instance;
  }
  public static HintCtrl GetHintCtrl() 
  {
    var curInstance = GetInstance();
    if (curInstance.hintCtrl == null)
      curInstance.hintCtrl = curInstance.GetComponentInChildren<HintCtrl>();
    return curInstance.hintCtrl;
  }
  void Awake()
  {
    instance = this;
    DontDestroyOnLoad(this.gameObject);

  }
  private void Start() {
    Init();

  }

  private void Init() 
  {
    hintCtrl.Init();

  }

    public void DisplayEnding()
    {
        endingPanel.SetActive(true);
    }

  public void DisplayDialog(string content,GameObject bindObj,float displayInterval) {

        
        var dialogObj = Instantiate(dialogPrefab,dialogContainer);
        dialogObj.transform.SetParent(dialogContainer);
        DialogBubble db = dialogObj.GetComponent<DialogBubble>();
        
        db.bindObject = bindObj;
        
        var t = dialogObj.gameObject.GetComponentInChildren<Text>();
        t.text = content;
        Destroy(dialogObj,displayInterval);
    
  }


}
