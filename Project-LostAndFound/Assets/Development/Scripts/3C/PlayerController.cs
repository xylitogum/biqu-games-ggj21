using UnityEngine;
using System.Collections;
using Prime31;
using MEC;
using System.Collections.Generic;
using AudioStudio;
using UnityEngine.Events;

namespace ThreeC
{
    public class PlayerController : MonoBehaviour
    {
        // movement config
        public float gravity = -25f;
        public float runSpeed = 8f;
        public float groundDamping = 20f; // how fast do we change direction? higher means faster
        public float inAirDamping = 5f;
        public float jumpHeight = 3f;

        [HideInInspector]
        private float normalizedHorizontalSpeed = 0;

        private CharacterController2D _controller;
        private Animator _animator;
        private RaycastHit2D _lastControllerColliderHit;
        private Vector3 _velocity;
        private Collider2D _collider;
        //private int _interactableLayerMask;
        public float globalAnimationSpeed = 1f;
        [Header("Interaction")]
        public InteractableBehaviour currentInteractable;
        public bool interacting = false;
        public float interactTime;
        public UnityEvent<bool, float> onInteractTimeChangeEvent; // ���ڽ�������������
        public UnityEvent<bool> onInteractButtonStateChangeEvent; // E��ʾ״̬
        public UnityEvent<bool> onSecondaryButtonStateChangeEvent; // F��ʾ״̬ (TODO)
        public GameObject failureFX;

        public GameObject sideBody;
        public GameObject frontBody;
        void Awake()
        {
            if (GameManager.Instance) GameManager.Instance.playerController = this;

            //_interactableLayerMask = 1 << LayerMask.NameToLayer("Interactable");
            _animator = GetComponent<Animator>();
            _controller = GetComponent<CharacterController2D>();

            // listen to some events for illustration purposes
            _controller.onControllerCollidedEvent += onControllerCollider;
            _controller.onTriggerEnterEvent += onTriggerEnterEvent;
            _controller.onTriggerExitEvent += onTriggerExitEvent;
            _collider = GetComponent<Collider2D>();

        }

        private void Start()
        {
            _animator.speed = globalAnimationSpeed;
        }

        #region Event Listeners

        void onControllerCollider(RaycastHit2D hit)
        {
            // bail out on plain old ground hits cause they arent very interesting
            if (hit.normal.y == 1f)
                return;

            // logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
            //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
        }


        void onTriggerEnterEvent(Collider2D col)
        {
            Debug.Log("onTriggerEnterEvent: " + col.gameObject.name);
        }


        void onTriggerExitEvent(Collider2D col)
        {
            Debug.Log("onTriggerExitEvent: " + col.gameObject.name);
        }

        #endregion


        // the Update loop contains a very simple example of moving the character around and controlling the animation
        void Update()
        {
            if (_controller.isGrounded)
                _velocity.y = 0;

            _animator.SetBool("Walking", false);
            Debug.Log(Input.GetAxis("Horizontal"));
            if (Input.GetAxis("Horizontal") > 0f)
            {
                normalizedHorizontalSpeed = 1;
                if (transform.localScale.x > 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                {
                    //_animator.Play(Animator.StringToHash("Run"));
                    _animator.SetBool("Walking", true);
                }

            }
            else if (Input.GetAxis("Horizontal") < 0f)
            {
                normalizedHorizontalSpeed = -1;
                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                if (_controller.isGrounded)
                {
                    //_animator.Play(Animator.StringToHash("Run"));
                    _animator.SetBool("Walking", true);
                }

            }
            else
            {
                normalizedHorizontalSpeed = 0;

                if (_controller.isGrounded)
                {
                    //_animator.Play(Animator.StringToHash("Idle"));
                }

            }


            //// we can only jump whilst grounded
            //if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
            //{
            //    _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            //    _animator.Play(Animator.StringToHash("Jump"));
            //}


            // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
            var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
            _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

            // apply gravity before moving
            _velocity.y += gravity * Time.deltaTime;

            // if holding down bump up our movement amount and turn off one way platform detection for a frame.
            // this lets us jump down through one way platforms
            //if (_controller.isGrounded && Input.GetKey(KeyCode.DownArrow))
            //{
            //	_velocity.y *= 3f;
            //	_controller.ignoreOneWayPlatformsThisFrame = true;
            //}

            _controller.move(_velocity * Time.deltaTime);

            // grab our current _velocity to use as a base for all calculations
            _velocity = _controller.velocity;



            UpdateInteract();

            if (Input.GetKeyDown(KeyCode.K))
            {
                StartPlayCD();


            }
        }

        public void StartPlayCD()
        {
            sideBody.SetActive(false);
            frontBody.SetActive(true);
            _animator.SetTrigger("Play");

        }
        //IEnumerator OnButtonHoldCoroutine()
        //{

        //}
        private void UpdateInteract()
        {
            KeyCode interactKey = KeyCode.E;
            KeyCode secondaryInteractKey = KeyCode.F;
            if (currentInteractable)
            {
                onInteractButtonStateChangeEvent.Invoke(true);
            }
            else
            {
                onInteractButtonStateChangeEvent.Invoke(false);
            }
            if (interacting)
            {
                if (currentInteractable && currentInteractable.canInteract)
                {
                    if (Input.GetKey(interactKey))
                    {
                        float prevInteractTime = interactTime;
                        // holding
                        interactTime += Time.deltaTime;
                        onInteractTimeChangeEvent.Invoke(true, GetInteractRatio());
                        if (currentInteractable.interactExtendDuration > 0.001f)
                        {
                            // can overflow
                            if (interactTime >= currentInteractable.interactDuration + currentInteractable.interactExtendDuration)
                            {
                                // did overflow
                                StopInteract(false);
                                SpawnFailureFX();
                                return;
                            }
                            else if (currentInteractable.requireSecondary && interactTime >= currentInteractable.interactDuration)
                            {
                                // In extended zone
                                onSecondaryButtonStateChangeEvent.Invoke(true);
                            }
                        }
                        else
                        {
                            // can't overflow
                            if (interactTime >= currentInteractable.interactDuration)
                            {
                                // gtg
                                StopInteract(true);
                                return;
                            }
                        }
                        float interruptTiming = currentInteractable.checkInterruptRatio * currentInteractable.interactDuration;
                        if (currentInteractable.requireInterruptCount > 0 && prevInteractTime < interruptTiming && interactTime >= interruptTiming) // can interrupt and is at interrupt timing
                        {
                            currentInteractable.requireInterruptCount -=1;
                            StopInteract(false);
                            SpawnFailureFX();
                            return;
                        }

                    }
                    else if (Input.GetKeyUp(interactKey))
                    {
                        // release
                        if (!currentInteractable.requireSecondary && currentInteractable.interactExtendDuration > 0.001f)
                        {
                            // can overflow
                            if (interactTime >= currentInteractable.interactDuration && interactTime < currentInteractable.interactDuration + currentInteractable.interactExtendDuration)
                            {
                                // In extended zone
                                StopInteract(true);
                                return;
                            }
                            else
                            {
                                StopInteract(false);
                                return;
                            }
                        }
                        else
                        {
                            // can't overflow
                            StopInteract(false);
                            return;
                        }
                    }

                    if (Input.GetKeyUp(secondaryInteractKey))
                    {
                        if (currentInteractable.requireSecondary && currentInteractable.interactExtendDuration > 0.001f)
                        {
                            // can overflow
                            if (interactTime >= currentInteractable.interactDuration && interactTime < currentInteractable.interactDuration + currentInteractable.interactExtendDuration)
                            {
                                // In extended zone
                                StopInteract(true);
                                return;
                            }
                            else
                            {
                                // wrong time
                                StopInteract(false);
                                return;
                            }
                        }
                    }
                }
                else
                {
                    StopInteract(false);
                    return;
                }
                
                
            }
            else if (Input.GetKeyDown(interactKey))
            {
                // start
                TryStartInteract();
            }
        }

        public float GetInteractRatio()
        {
            if (interacting && currentInteractable && currentInteractable.interactDuration != 0f)
            {
                return interactTime / currentInteractable.interactDuration;
            }
            else
            {
                return -1f;
            }
        }

        public void PlayDeath()
        {
            SpawnFailureFX();
            sideBody.SetActive(true);
            frontBody.SetActive(false);
            _animator.SetTrigger("Death");
        }

        private void StopInteract(bool success)
        {
            interacting = false;
            interactTime = 0f;
            onInteractTimeChangeEvent.Invoke(false, 0f);
            onSecondaryButtonStateChangeEvent.Invoke(false);
            if (currentInteractable)
            {
                if (success)
                {
                    if (currentInteractable.shoudPlayCD)
                    {
                        StartPlayCD();
                    }
                    else
                    {
                        _animator.SetTrigger("InteractSuccess");
                    }
                    
                    currentInteractable.OnInteractSuccess(this);
                    
                }
                else
                {
                    _animator.SetTrigger("InteractFail");
                    currentInteractable.OnInteractFail(this);
                    
                }
            }
            
                
        }
        
        private void TryStartInteract()
        {
            onInteractTimeChangeEvent.Invoke(true, GetInteractRatio());
            if (currentInteractable && currentInteractable.canInteract)
            {
                interacting = true;
                interactTime = 0f;
                _animator.SetBool("InteractStart", true);

            }
            AudioManager.PlaySound("UI_Interact_Begin");
        }

        private void SpawnFailureFX()
        {
            if (failureFX)
            {
                Instantiate(failureFX, transform.position, Quaternion.identity);
            }
        }
    }
}