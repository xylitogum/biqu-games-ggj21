using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace ThreeC
{
    public class CameraSnap : MonoBehaviour
    {



        public List<CinemachineVirtualCamera> vCams;
        [Header("Prototyping Only")]
        public bool useKeyToSnap;

        private void Awake()
        {

        }
        // Start is called before the first frame update
        void Start()
        {
            GameManager.Instance.OnRoomSwitchEvent.AddListener(SetWeight);
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void SetWeight(int index)
        {
            for (int i = 0; i < vCams.Count; i++)
            {
                if (i != index)
                    vCams[i].gameObject.SetActive(false);
                else
                    vCams[i].gameObject.SetActive(true);
            }
        }
    }
}
