using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThreeC
{
    public class DebugInteractEvent : MonoBehaviour
    {
        public void DebugString(PlayerController context, InteractableBehaviour interactable)
        {
            Debug.Log("Executed!");
        }
    }

}