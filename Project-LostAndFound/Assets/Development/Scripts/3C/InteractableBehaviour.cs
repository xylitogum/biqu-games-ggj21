using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace ThreeC
{
    public class InteractableBehaviour : MonoBehaviour
    {

        public bool canInteract
        {
            get
            {
                return _canInteract;
            }
            set
            {
                _canInteract = value;
            }
        }
        [SerializeField]
        private bool _canInteract;
        public bool _inRange = false;
        public float interactDuration = 3f;
        public float interactExtendDuration = 0f;
        public bool requireSecondary = false;
        public int requireInterruptCount = 0;
        public float checkInterruptRatio = 0.5f;

        public bool shoudPlayCD = false;
        private void Awake()
        {
            canInteract = true;
        }


        public UnityEvent<InteractableBehaviour> OnInteractSuccessEvent;
        public UnityEvent<InteractableBehaviour> OnInteractFailEvent;

        public void OnInteractSuccess(PlayerController pc)
        {
            Debug.Log("Interact Success");
            OnInteractSuccessEvent.Invoke(this);
        }

        public void OnInteractFail(PlayerController pc)
        {
            Debug.Log("Interact Fail");
            OnInteractFailEvent.Invoke(this);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("Interactable in Range");
            _inRange = true;
            PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
            if (pc)
            {
                pc.currentInteractable = this;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            Debug.Log("Interactable out of Range");
            _inRange = false;
            PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
            if (pc && pc.currentInteractable == this)
            {
                pc.currentInteractable = null;
            }
        }
    }
}