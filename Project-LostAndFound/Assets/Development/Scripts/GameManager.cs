﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameLevel;
using ThreeC;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private CutsceneControl _cutsceneControl;
    public UnityEvent doEndEvent;
    public CutsceneControl CutsceneControl
    {
        get => _cutsceneControl;
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;

#if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying)
            return;
#endif

        DontDestroyOnLoad(gameObject);

        if (_cutsceneControl == null)
        {
            var cutscene = GameObject.Find("Cutscene");
            _cutsceneControl = cutscene.GetComponent<CutsceneControl>();

            DontDestroyOnLoad(cutscene);
            DontDestroyOnLoad(GameObject.Find("EventSystem"));
        }
    }

    private void Update()
    {
        if (_level != null)
        {
            _level.Update(Time.deltaTime, Time.unscaledDeltaTime);
        }

        var roomState = _level?.LevelStateMachine.CurrentLevelState as RoomState;
        if (roomState != null)
        {
            _currentRoomIndex = roomState.roomIndex;
        }

        if (allowDebugSwitch)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                ChangeToNextLevel();
            }
        }
    }

    public void Reset()
    {
        _level = null;
    }

    public void OnLevelEnd()
    {
        ChangeLevelState<LevelEndState>();
    }

    public void ChangeLevelState<TState>() where TState : LevelState
    {
        _level.LevelStateMachine.ChangeState<TState>();
    }
    
    public LevelState GetCurrentLevelState()
    {
        return _level.GetCurrentLevelState();
    }
    
    public string GetCurrentLevelStateName()
    {
        return _level.GetCurrentLevelState() != null ? _level.GetCurrentLevelState().GetType().FullName : null;
    }

    #region Level

    public bool allowDebugSwitch = true;

    [FormerlySerializedAs("sceneNames")] public string[] _sceneNames;

    private Level _level;

    public Level Level
    {
        get => _level;
    }

    public string GetCurrentLevelSceneName()
    {
        return _sceneNames[_level.Index - 1];
    }

    public void ChangeToNextLevel()
    {
        if (_sceneNames == null)
        {
            Debug.LogError("SceneNames invalid!");
            return;
        }

        int curLevelIndex = _level == null ? 0 : _level.Index;

        if (curLevelIndex < _sceneNames.Length)
        {
            ChangeScene(_sceneNames[curLevelIndex]);
            _level = new Level(curLevelIndex + 1);
        }
        else
        {
            DoEnd();
        }
    }
    
    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    private void DoEnd()
    {
        doEndEvent.Invoke();
    }
    
    #endregion

    #region Room

    private int _currentRoomIndex;
    public UnityEvent<int> OnRoomSwitchEvent;
    public int CurrentRoomIndex
    {
        get => _currentRoomIndex;
    }

    public RoomData GetRoom(int roomIndex)
    {
        return _level.LevelConfig.rooms[roomIndex];
    }

    public void ChangeRoom(int roomIndex, GameObject playerObj)
    {
        var state = _level.LevelStateMachine.GetState<RoomChangingState>() as RoomChangingState;
        if (state != null)
        {
            OnRoomSwitchEvent.Invoke(roomIndex);
            state.targetRoom = roomIndex;
            ChangeLevelState<RoomChangingState>();
        }
    }

    #endregion

    #region Player
    [HideInInspector]
    public ThreeC.PlayerController playerController;
    #endregion

    #region Interactable

    public Interactable GetInteractable(int index)
    {
        if (_level == null) return null;

        return _level.GetInteractable(index);
    }

    public bool IsInteractableCompleted(int index)
    {
        return _level.IsInteractableCompleted(index);
    }

    public void AddSuccessListener(int index, UnityAction<InteractableBehaviour> action)
    {
        _level.AddSuccessListener(index, action);
    }
    
    #endregion

    #region HUD

    public void DisplayMissionHint(string hint)
    {
        var hud = HUD.GetInstance();
        if (hud == null) return;
        hud.GetComponentInChildren<HintCtrl>()?.DisplayMissionHint(hint, true);
    }
    
    public void HideMissionHint()
    {
        var hud = HUD.GetInstance();
        if (hud == null) return;
        hud.GetComponentInChildren<HintCtrl>()?.DisplayMissionHint("", false);
    }
    
    public void DisplayDirHint(HintCtrl.DirectionHint dir)
    {
        var hud = HUD.GetInstance();
        if (hud == null) return;
        hud.GetComponentInChildren<HintCtrl>()?.DisplayDirHint(dir);
    }

    #endregion
}
