using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ending : MonoBehaviour
{
    public HUD hud;
    private float timeElapsed;
    public float timeDisplayDeath;
    public float timeDisplayBlack;
    public float timeDisplayEndingPanel;
    public float timeDisplayCredit;
    // Start is called before the first frame update
    void Start()
    {
        OnEnding();
        timeElapsed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        float prevTime = timeElapsed;
        timeElapsed += Time.deltaTime;
        if (prevTime < timeDisplayDeath && timeElapsed >= timeDisplayDeath)
        {
            GameManager.Instance.playerController.PlayDeath();
        }
        if (prevTime < timeDisplayBlack && timeElapsed >= timeDisplayBlack)
        {
            Camera.main.cullingMask = LayerMask.GetMask("Player");
        }
        if (prevTime < timeDisplayEndingPanel && timeElapsed >= timeDisplayEndingPanel)
        {
            hud.DisplayEnding();
        }
        if (prevTime < timeDisplayCredit && timeElapsed >= timeDisplayCredit)
        {
            GameManager.Instance.CutsceneControl.credit.gameObject.SetActive(true);
            GameManager.Instance.CutsceneControl.main.SetActive(true);
            GameManager.Instance.Reset();
        }
    }

    public void OnEnding()
    {
        
        
        
    }
}
