using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTeleport : MonoBehaviour
{
    public bool active = true;
    public int targetRoomIndex;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Teleport");
        if (active)
        {
            GameManager.Instance.ChangeRoom(targetRoomIndex, collision.gameObject);
            
        }
        
    }
}
