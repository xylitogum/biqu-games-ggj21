using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[ExecuteInEditMode]
public class Fire : MonoBehaviour
{
    public string sortingLayerName = "Foreground";
    public int sortingOrder;

    public Light2D pointLight;
    public float pointLightIntensity = 1.0f;

    public Light2D freeformLight1;
    public float FreeformLightIntensity1 = 1.5f;

    public Light2D freeformLight2;
    public float FreeformLightIntensity2 = 0.4f;

    // Start is called before the first frame update
    void OnEnable()
    {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        mr.sortingLayerName = sortingLayerName;
        mr.sortingOrder = sortingOrder;

        pointLight = gameObject.transform.parent.Find("Fire PointLight 2D").GetComponent<Light2D>();
        freeformLight1 = gameObject.transform.parent.Find("Fire Freeform Light 2D 1").GetComponent<Light2D>();
        freeformLight2 = gameObject.transform.parent.Find("Fire Freeform Light 2D 2").GetComponent<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Mathf.Sin(Time.time * 7.0f) * 0.1f
                        + Mathf.Sin(Time.time * 15.0f) * 0.1f
                        + Mathf.Sin(Time.time * 30.0f) * 0.1f;
        pointLight.intensity = pointLightIntensity * (offset + 1.0f);
        freeformLight1.intensity = FreeformLightIntensity1 * (offset + 1.0f);
        freeformLight2.intensity = FreeformLightIntensity2 * (offset + 1.0f);
    }
}
