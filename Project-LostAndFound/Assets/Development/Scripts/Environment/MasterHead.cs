using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterHead : MonoBehaviour
{
    public HUD hud;
    public string MessageStart;
    public float MessageStartDuration = 2f;
    public string MessageEnd;
    public float MessageEndDuration = 2f;
    public GameObject followTarget;

    private Animator _animator;
    // Start is called before the first frame update
    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayStartMessage()
    {
        if (_animator)  _animator.SetTrigger("Speak");
        hud.DisplayDialog(MessageStart, followTarget, MessageStartDuration);
    }

    public void DisplayEndMessage()
    {
        if (_animator) _animator.SetTrigger("Speak");
        hud.DisplayDialog(MessageEnd, followTarget, MessageEndDuration);
    }
}
