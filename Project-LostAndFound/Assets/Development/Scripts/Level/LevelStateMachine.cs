﻿using System;
using System.Collections.Generic;
using UnityEngine.XR;

namespace GameLevel
{
    public class LevelStateMachine
    {
        private Level _owner;

        public Level Owner
        {
            get => _owner;
        }

        public LevelStateMachine(Level owner, List<LevelState> states)
        {
            this._owner = owner;
            _states = new Dictionary<Type, LevelState>();
            
            foreach (LevelState state in states)
            {
                if (state == null)
                {
                    continue;
                }

                Type stateType = state.GetType();
                if (_states.ContainsKey(stateType))
                {
                    continue;
                }

                _states.Add(stateType, state);
                state._stateMachine = this;
                state.OnInit();
            }
        }

        private LevelState _currentLevelState;

        public LevelState CurrentLevelState
        {
            get => _currentLevelState;
        }

        private float _currentStateTime;

        private Dictionary<Type, LevelState> _states;
        
        public void Start<TState>() where TState : LevelState
        {
            Start(typeof(TState));
        }
        
        public void Start(Type stateType)
        {
            if (stateType == null)
            {
                throw new ArgumentNullException();
            }

            LevelState state = GetState(stateType);
            if (state == null)
            {
                return;
            }

            _currentStateTime = 0f;
            _currentLevelState = state;
            _currentLevelState.UpdateStateTime(_currentStateTime);
            _currentLevelState.OnEnter();
        }
        
        public void Update(float elapseSeconds, float realElapseSeconds)
        {
            if (_currentLevelState == null)
            {
                return;
            }

            _currentStateTime += elapseSeconds;
            _currentLevelState.UpdateStateTime(_currentStateTime);
            _currentLevelState.OnUpdate(elapseSeconds, realElapseSeconds);
        }
        
        public void Clear()
        {
            if (_currentLevelState != null)
            {
                _currentLevelState.OnLeave();
            }

            foreach (KeyValuePair<Type, LevelState> state in _states)
            {
                state.Value.OnDestroy();
            }
            
            _currentLevelState = null;
            _currentStateTime = 0f;
        }
        
        public LevelState GetState<TState>() where TState : LevelState
        {
            return GetState(typeof(TState));
        }
        
        public LevelState GetState(Type stateType)
        {
            if (stateType == null)
            {
                throw new ArgumentNullException();
            }

            if (_states.TryGetValue(stateType, out var state))
            {
                return state;
            }

            return null;
        }

        public void ChangeState<TState>() where TState : LevelState
        {
            ChangeState(typeof(TState));
        }
        
        public void ChangeState(Type stateType)
        {
            if (stateType == null)
            {
                throw new ArgumentNullException();
            }

            LevelState state = GetState(stateType);
            if (state == null)
            {
                return;
            }

            _currentLevelState?.OnLeave();
            _currentStateTime = 0f;
            _currentLevelState = state;
            _currentLevelState.UpdateStateTime(_currentStateTime);
            _currentLevelState.OnEnter();
        }
    }
}
