﻿using System;
using Cinemachine;
using ThreeC;
using UnityEngine;

namespace GameLevel
{
    [Serializable]
    public class RoomData
    {
        public int index;
        public Transform bornPoint;
        public CinemachineVirtualCamera cinemachine;
    }

    [Serializable]
    public class Interactable
    {
        public int index;
        public HintCtrl.DirectionHint direction;
        public InteractableBehaviour behaviour;
        public int[] predecessorIndex;
        public bool available;
        public bool levelSuccess;
        public string hint;
        [HideInInspector]
        public bool completed = false;
    }
    
    public class LevelConfig : MonoBehaviour
    {
        [SerializeField]
        public RoomData[] rooms;

        public int defaultRoomIndex = 0;

        [SerializeField]
        public Interactable[] interactables;
    }
}