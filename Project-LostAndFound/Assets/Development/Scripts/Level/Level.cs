﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ThreeC;
using UnityEngine;
using UnityEngine.Events;

namespace GameLevel
{
    public class Level
    {
        private int index;

        public int Index
        {
            get => index;
        }

        private readonly LevelStateMachine _levelStateMachine;

        public LevelStateMachine LevelStateMachine
        {
            get => _levelStateMachine;
        }

        private LevelConfig _levelConfig;
        
        public LevelConfig LevelConfig
        {
            get => _levelConfig;
        }

        public void LoadLevelConfig()
        {
            var configObj = GameObject.Find("LevelConfig");
            _levelConfig = configObj.GetComponent<LevelConfig>();

            if (_levelConfig.interactables != null)
            {
                foreach (var interactable in _levelConfig.interactables)
                {
                    if (interactable.behaviour == null) continue;
                    interactable.completed = false;
                    interactable.behaviour.OnInteractSuccessEvent.AddListener((behaviour) => SetInteractableSuccess(interactable.index, behaviour));
                }
                InitializeAvailable();
            }
        }

        #region Interactable

        public Interactable GetInteractable(int index)
        {
            if (_levelConfig == null) return null;

            foreach (var interactable in _levelConfig.interactables)
            {
                if (interactable.index == index)
                {
                    return interactable;
                }
            }

            return null;
        }

        public bool IsInteractableCompleted(int index)
        {
            var interactable = GetInteractable(index);
            if (interactable == null) return false;

            return interactable.completed;
        }

        private void InitializeAvailable()
        {
            StringBuilder hint = new StringBuilder("");
            
            foreach (var interactable in _levelConfig.interactables)
            {
                if (interactable.behaviour == null) continue;

                interactable.behaviour.gameObject.SetActive(interactable.available);

                if (interactable.available && !interactable.completed)
                {
                    if (hint.Length > 0) hint.Append("\n");
                    hint.Append(interactable.hint);
                }
            }
            
            GameManager.Instance.DisplayMissionHint(hint.ToString());
        }

        private void UpdateAvailable(Interactable interactable)
        {
            if (interactable == null || interactable.behaviour == null) return;
            if (interactable.available || interactable.completed) return;
            
            foreach (var pre in interactable.predecessorIndex)
            {
                if (!IsInteractableCompleted(pre))
                {
                    return;
                }
            }

            interactable.available = true;
            interactable.behaviour.gameObject.SetActive(true);
        }
        
        private void SetInteractableSuccess(int index, InteractableBehaviour behaviour)
        {
            var interactable = GetInteractable(index);
            if (interactable == null) return;
            
            interactable.completed = true;
            behaviour.gameObject.SetActive(false);
            
            StringBuilder hint = new StringBuilder("");
            foreach (var post in _levelConfig.interactables)
            {
                UpdateAvailable(post);
                
                if (post.available && !post.completed)
                {
                    if (hint.Length > 0) hint.Append("\n");
                    hint.Append(post.hint);
                }
            }
            
            GameManager.Instance.DisplayMissionHint(hint.ToString());

            if (interactable.levelSuccess)
            {
                GameManager.Instance.OnLevelEnd();
            }
        }

        public void AddSuccessListener(int index, UnityAction<InteractableBehaviour> action)
        {
            var interactable = GetInteractable(index);
            if (interactable == null || interactable.behaviour == null) return;
            
            interactable.behaviour.OnInteractSuccessEvent.AddListener(action);
        }

        #endregion

        public Level(int index)
        {
            this.index = index;

            _levelStateMachine = new LevelStateMachine(this, RegisterLevelStates());
            var state = _levelStateMachine.GetState<LevelLoadingState>() as LevelLoadingState;
            state.level = index;
            _levelStateMachine.Start<LevelLoadingState>();
        }

        public void Update(float elapseSeconds, float realElapseSeconds)
        {
            _levelStateMachine?.Update(elapseSeconds, realElapseSeconds);
            var levelConfig = GameManager.Instance.Level.LevelConfig;
            foreach (var interactable in levelConfig.interactables)
            {
                if (interactable.available == true && interactable.completed == false)
                {
                    ThreeC.InteractableBehaviour ib = interactable.behaviour;
                    var playerObj = GameManager.Instance.playerController;
                    if (!playerObj.currentInteractable)
                    {
                        GameManager.Instance.DisplayDirHint(interactable.direction);
                    }
                    else
                    {
                        GameManager.Instance.DisplayDirHint(HintCtrl.DirectionHint.None);
                    }
                }
            }
        }
        
        private List<LevelState> RegisterLevelStates()
        {
            List<LevelState> states = new List<LevelState>();
            
            var types = Assembly.GetCallingAssembly().GetTypes();
            var cType = typeof(LevelState);

            foreach (var type in types)
            {
                var baseType = type.BaseType;
                while (baseType != null)
                {
                    if (baseType.Equals(cType))
                    {
                        object obj = System.Activator.CreateInstance(type);
                        if (obj != null)
                        {
                            LevelState info = obj as LevelState;
                            states.Add(info);
                        }
                        break;
                    }

                    baseType = baseType.BaseType;
                }
            }

            return states;
        }

        public LevelState GetCurrentLevelState()
        {
            return _levelStateMachine.CurrentLevelState;
        }
    }
}
