﻿namespace GameLevel
{
    public abstract class LevelState
    {
        internal LevelStateMachine _stateMachine;

        public LevelStateMachine StateMachine
        {
            get => _stateMachine;
        }

        protected float _stateTime = 0f;
            
        public virtual void OnInit()
        {
            
        }
        
        public virtual void OnEnter()
        {
            
        }

        public virtual void OnUpdate(float elapseSeconds, float realElapseSeconds)
        {
            
        }
        
        public virtual void OnLeave()
        {
            
        }
        
        public virtual void OnDestroy()
        {
            
        }
        
        public void ChangeState<TState>() where TState : LevelState
        {
            _stateMachine.ChangeState(typeof(TState));
        }

        public void UpdateStateTime(float time)
        {
            _stateTime = time;
        }
    }
}