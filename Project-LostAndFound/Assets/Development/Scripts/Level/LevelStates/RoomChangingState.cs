using GameLevel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomChangingState : LevelState
{
    private const float time = 0f;

    public int targetRoom;
    
    public override void OnEnter()
    {

    }

    public override void OnUpdate(float elapseSeconds, float realElapseSeconds)
    {
        if (_stateTime > time)
        {
            var roomState = StateMachine.GetState<RoomState>() as RoomState;
            if (roomState != null)
            {
                roomState.roomIndex = targetRoom;
                roomState.first = false;
                ChangeState<RoomState>();
            }
        }
    }

    public override void OnLeave()
    {
        
    }
}