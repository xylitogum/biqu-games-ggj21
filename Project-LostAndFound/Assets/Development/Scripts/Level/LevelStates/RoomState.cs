﻿using GameLevel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomState : LevelState
{
    public int roomIndex;

    public bool first;
    
    public override void OnEnter()
    {
        var levelConfig = GameManager.Instance.Level.LevelConfig;
        foreach (var roomData in levelConfig.rooms)
        {
            if (roomData.index == roomIndex)
            {
                var playerObj = GameManager.Instance.playerController;
                if (!first) playerObj.transform.position = roomData.bornPoint.position;
                roomData.cinemachine.gameObject.SetActive(true);
            }
            else
            {
                roomData.cinemachine.gameObject.SetActive(false);
            }
        }
    }

    public override void OnUpdate(float elapseSeconds, float realElapseSeconds)
    {
       
        
    }

    public override void OnLeave()
    {
        GameManager.Instance.DisplayDirHint(HintCtrl.DirectionHint.None);
    }
}