using System.Net.Configuration;
using AudioStudio;
using GameLevel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoadingState : LevelState
{
    private bool prepare = false;

    public int level = 1;

    public override void OnEnter()
    {
        prepare = false;
        if (GameManager.Instance.CutsceneControl != null) 
            GameManager.Instance.CutsceneControl.ShowCutscene(level);
        GameManager.Instance.HideMissionHint();
    }

    public override void OnUpdate(float elapseSeconds, float realElapseSeconds)
    {
        if (SceneManager.GetActiveScene().name == GameManager.Instance.GetCurrentLevelSceneName())
        {
            prepare = true;
        }

        if (prepare && Input.anyKeyDown)
        {
            ChangeState<LevelBeginState>();
        }
    }

    public override void OnLeave()
    {
        if (GameManager.Instance.CutsceneControl != null) 
            GameManager.Instance.CutsceneControl.HideAllCutscene();
        _stateMachine.Owner.LoadLevelConfig();
        AudioManager.PlayMusic("Music_Main");
    }
}