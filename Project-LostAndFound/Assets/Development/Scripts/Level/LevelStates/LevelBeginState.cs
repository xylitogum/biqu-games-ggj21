﻿using GameLevel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelBeginState : LevelState
{
    private const float time = 2f;
    
    public override void OnEnter()
    {

    }

    public override void OnUpdate(float elapseSeconds, float realElapseSeconds)
    {
        if (GameManager.Instance.CutsceneControl != null) 
            GameManager.Instance.CutsceneControl.SetBlackFade(Mathf.Pow(Mathf.Lerp(1f, 0, _stateTime / time), 0.7f));
        
        if (_stateTime > time)
        {
            var roomState = StateMachine.GetState<RoomState>() as RoomState;
            if (roomState != null)
            {
                roomState.roomIndex = GameManager.Instance.Level.LevelConfig.defaultRoomIndex;
                roomState.first = true;
                ChangeState<RoomState>();
            }
        }
    }

    public override void OnLeave()
    {
        
    }
}
