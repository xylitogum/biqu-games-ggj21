﻿using GameLevel;
using UnityEngine;

public class LevelEndState : LevelState
{
    private const float time = 5f;
    
    public override void OnEnter()
    {

    }

    public override void OnUpdate(float elapseSeconds, float realElapseSeconds)
    {
        
        
        if (GameManager.Instance.Level.Index == 3)
        {
            
            if (_stateTime > time + 12f)
            {
                StateMachine.Clear();
            }
        }
        else
        {
            if (GameManager.Instance.CutsceneControl != null)
                GameManager.Instance.CutsceneControl.SetBlackFade(Mathf.Pow(Mathf.Lerp(0, 1f, _stateTime / time), 0.7f));
            if (_stateTime > time)
            {
                StateMachine.Clear();
            }
        }
        
    }

    public override void OnLeave()
    {
        GameManager.Instance.ChangeToNextLevel();
    }
}