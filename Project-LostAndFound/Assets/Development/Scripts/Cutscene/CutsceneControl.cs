﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class CutsceneControl : MonoBehaviour
{
    public Image black;
    public Image[] cutscenes;
    public Image credit;
    public GameObject main;

    public float vinylSpeed;
    public Image vinyl;
    public Button startButton;
    public Button creditButton;
    public Button quitButton;

    private void Awake()
    {
        startButton.onClick.AddListener(() =>
        {
            GameManager.Instance.ChangeToNextLevel();
            main.gameObject.SetActive(false);
        });
        
        creditButton.onClick.AddListener(() =>
        {
            credit.gameObject.SetActive(true);
        });
        
        quitButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }

    private void Update()
    {
        if (credit.gameObject.activeSelf && Input.anyKeyDown)
        {
            credit.gameObject.SetActive(false);
        }
        
        vinyl.gameObject.transform.Rotate(Vector3.forward, vinylSpeed * Time.deltaTime);
    }

    public void HideAllCutscene()
    {
        foreach (var cutscene in cutscenes)
        {
            cutscene.gameObject.SetActive(false);
        }
    }
    
    public void ShowCutscene(int index)
    {
        if (index < 0 || index > cutscenes.Length) return;
        HideAllCutscene();
        cutscenes[index - 1].gameObject.SetActive(true);
    }

    public void SetBlackFade(float alpha)
    {
        if (alpha <= 0) black.gameObject.SetActive(false);
        else
        {
            black.gameObject.SetActive(true);
            black.color = new Color(black.color.r, black.color.g, black.color.b, alpha);
        }
    }
}
